new_group 50
Question
========

RQ: Is there a correlation between the total_runs and  ball? 

Dependent variable : total_runs
Independent variable:ball

Null hypothesis: There is no equivalence between total_runs and ball.

Alternative hypothesis: The total_runs and ball are equivalent.
Data Column: Index, total_runs, ball.

Dataset
=======

URL: https://www.kaggle.com/patrickb1912/ipl-complete-dataset-20082020
read.csv("research_question.csv")

Column Headings:
 
[1] "id"               "inning"           "over"             "ball"             "batsman"          "non_striker"     
 [7] "bowler"           "batsman_runs"     "extra_runs"       "total_runs"       "non_boundary"     "is_wicket"       
[13] "dismissal_kind"   "player_dismissed" "fielder"          "extras_type"      "batting_team"     "bowling_team"    
